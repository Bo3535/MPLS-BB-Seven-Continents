### Table of contents
- [Introduction](#introduction)
    - [Overview](#overview)
    - [Performance](#performance)
    - [Security](#security)
    - [Hardware/Software](#hardwaresoftware)
    - [Protocols and standards](#protocols-and-standards)
    - [IP layout](#ip-layout)
    - [Naming convention](#naming-convention)

<!-- pagebreak -->


# Introduction
This document describes the MPLS backbone implemented by the five consultant groups of the "The 5 Continents” project.  This document briefly describes the protocols, standards and other relevant information being used in the core network.
	The MPLS network connects 6 continent sites: Brazil, Russia, Africa, Australia, Europe, Asia. Five of these sites are autonomous networks which are all connected to Brazil which acts as a server farm. The MPLS network is running IPv4 as the underlying layer 3 protocol, but it also tunnels the continent IPv6 traffic through the core.

<!-- pagebreak -->


## Overview

<img src="../diagrams/topology-mpls.jpg">

<!-- pagebreak -->


## Performance

We only for see one problem which may be the reservation of bandwidt by MPLS, but other then that limittation we do not see any performance problems. We are using somewhat the most possible bandwidth for traffic engineering, in our backbone. There isn't any kind of traffic that needs higher priovity going thorugh the backbone, so all traffic is handled indentical, and equal.
There are some performance constrains with the brazil site, using the routing protocol NAT64. NAT64 introduces two proxy addresses, which has this contrain that they don't activate automatically unless they recive an ICMP packet. This means that they have to be manually pinged before use.
For connection this is fixed by using static host mappings of domainnames to ips.

<!-- pagebreak -->


## Security

We haven't done much to secure the backbone, we did not see it relevant because it's not on the internet. But since it's running MPLS it's secure aginst spoofing, because of the insolation. Everything is set to packet-based of the SRXs, therefore there won't be any statefull traffic. We have no reason to block any ips or traffic. 
For password mangement, there are an ssh-key loaded to all the devices inside the MPLS Backbone, for the automation server to use netconf without having a password stored. It is up to the individual group to secure their own pe router with passwords and keys. <br>

<!-- pagebreak -->


## Hardware/Software

12 x SRX240<br>
1 x blade server<br>
7 x Debian vm's<br>
1 x vSRX 17<br>
<table>
 <tr>
  <th>Router name</th>
  <th>version of JUNOS</th>
 </tr>
 <tr>
  <td>PE1</td>
  <td>12.1X46-D55.3;</td></tr>
  <tr>
  <td>PE2</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>PE3</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>PE4</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>PE5</td>
  <td>12.1X46-D30.2</td>
 </tr>
  <tr>
  <td>PE6</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>P1</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>P2</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>P3</td>
  <td>12.1X46-D55.3;</td>
 </tr>
  <tr>
  <td>P4</td>
  <td>12.1X46-D55.3;</td>
 </tr>
  <tr>
  <td>P5</td>
  <td>12.1X46-D30.2;</td>
 </tr>
  <tr>
  <td>P6</td><td>12.1X46-D55.3;</td></tr>
</table>

<!-- pagebreak -->


## Protocols and standards

<table>
 <tr>
<th>Protocol</th>
  <th>Standard</th>
  <th>Description</th>
 </tr>
 <tr>
  <td>IS-IS</td>
  <td><a href="https://tools.ietf.org/html/rfc1142">rfc1142</a></td>
  <td>This protocol is used as our IGP inside the backbone to calculate the shortests paths, and traffic engineering.
 </tr>
  <tr>
  <td>BGP</td>
  <td><a href="https://tools.ietf.org/html/rfc4271">rfc4271</a></td>
  <td> Used for reading the mpls.3 table and connecting our sites to the backbone thorugh EBGP.</td>
  <tr>
  <td>MPLS</td>
  <td><a href="https://tools.ietf.org/html/rfc3031">rfc3031</a></td>
  <td>The protocol used to deploy and handle the mpls label switching standard inside our backbone. The defined LSP will be configured under this signal protocol.</td>
 </tr>
 <tr>
  <td>RSVP</td>
  <td><a href="https://tools.ietf.org/html/rfc2205">rfc2205</a></td>
  <td>The protocol being used for reserving bandwidth, by using traffic engineering. </td>
 </tr>
  <tr>
  <td>Link Aggregation(LAG)</td>
  <td><a href="http://www.ieee802.org/3/hssg/public/apr07/frazier_01_0407.pdf">IEEE 802.3ad</a></td>
  <td>  Standard used for peer-bounding the interfaces into a single link.</td>
 </tr>
   <tr>
  <td>LACP</td>
  <td><a href="http://www.ieee802.org/3/hssg/public/apr07/frazier_01_0407.pdf">IEEE 802.3ad</a></td>
  <td> The protocl used for controlling the peered links, speed, load balacing and handling links that goes down.</td>
  </tr>
   <tr>
  <td>SSH</td>
  <td><a href="https://tools.ietf.org/html/rfc4253">rfc4253</a></td>
  <td> The connection protocol used for remote connection to devices, this is used with public key encryption, so the traffic will be secure. The keys also handles login instead of passwords.</td>x   
 </tr>
   <tr>
  <td>NETCONF</td>
  <td><a href="https://tools.ietf.org/html/rfc6241">rfc6241</a></td>
  <td>The network management protocol, ansible and python can communicate with the routers within the Backbone. This is used for our automatic operations.</td>
 </tr>
   <tr>
  <td>IPv6</td>
  <td><a href="https://www.ietf.org/rfc/rfc2460.txt">rfc2460</a></td>
  <td> The ip family used by the sites, and the pe routers interface facing the sites.</td>
 </tr>
 <tr>
  <td>IPv4</td>
  <td><a href="https://tools.ietf.org/html/rfc791">rfc791</a></td>
  <td>IP family used within the mpls backbone and internally in the brazil site.</td>
 </tr>
    <tr>
  <td>DNS64</td>
  <td><a href="https://www.ietf.org/rfc/rfc1035.txt">rfc1035</a></td>
  <td>Used for domain names, for reachbilty with both ipv6 addresses and ipv4 address.</td>
 </tr>
 <tr>
  <td>HTTP</td>
  <td><a href="https://www.ietf.org/rfc/rfc2616.txt">rfc2616</a></td>
  <td>Protcol used for connection to web-server </td>
 </tr>
  <tr>
  <td>NAT64</td>
  <td><a href="https://tools.ietf.org/html/rfc6146">rfc6146</a></td>
  <td>Used to make connectivity from ipv6 > ipv4 in the brazil site with dns servers</td>
 </tr>
  <tr>
  <td>6PE</td>
  <td><a href="https://tools.ietf.org/html/rfc4798">rfc4798</a></td>
  <td> Used for IPV6 tunnel thorugh the ipv4 based MPLS network.</td>
 </tr>
 <tr>
  <td>ICMP</td>
  <td><a href="https://tools.ietf.org/html/rfc792">rfc792</a></td>
  <td> Used for checking connectivity between routers and devices.</td>
 </tr>
 <tr>
  <td>OSPF</td>
  <td><a href="https://tools.ietf.org/html/rfc5340">rfc5340</a></td>
  <td>Used internally in the brazil site, to provide end-systems connectivity for other rotuers.</td>
 </tr>
 <tr>
  <td>NDP</td>
  <td><a href="https://tools.ietf.org/html/rfc4861">rfc4861</a></td>
  <td>Used for Dicovery of ipv6 address not presented in the current routing table. This is used as a proxy in nat64.</td>
 </tr>
 <tr>
  <td>ARP</td>
  <td><a href="https://tools.ietf.org/html/rfc826">rfc826</a></td>
  <td>Used for Dicovery of ipv6 address not presented in the current routing table. This is used as a proxy in nat64.</td >
 </tr>
</table>

<!-- pagebreak -->


## IP layout 
<table>
    <tr>
        <th></th>
        <th>IP</th>
        <th>subnet mask</th>
    </tr>
    <tr>
        <td>AE links</td>
        <td>192.168.75.0</td>
        <td>/26</td>
    </tr>
    <tr>
        <td>Backup links</td>
        <td>192.168.100.0</td>
        <td>/27</td>
    </tr>
    <tr>
        <td>Brazill nat64 IPv6</td>
        <td>2001:0:2:20::0</td>
        <td>/120</td>
    </tr>
    <tr>
        <td>Brazill nat64 IPv4</td>
        <td>10.69.69.0</td>
        <td>/30</td>
    </tr>
    <tr>
        <td></td>
        <td>10.69.68.0</td>
        <td>/30</td>
    </tr>
    <tr>
        <td>Brazill servers</td>
        <td>10.69.67.0</td>
        <td>/28</td>
    </tr>
</table><br>
<table>
 <tr>
  <th>Router name</th>
  <th>LO0 IP</th>
  <th>ISO address</th>
 </tr>
 <tr>
  <td>PE1</td>
  <td>10.10.10.1</td>
  <td>49.0001.0000.0000.001e.00</td>
 </tr>
 <tr>
  <td>PE2</td>
  <td>10.10.20.1</td>
  <td>49.0001.0000.0000.001e.00</td>
 </tr>
 <tr>
  <td>PE3</td>
  <td>10.10.30.1</td>
  <td>49.0001.0000.0000.003e.00</td>
 </tr>
 <tr>
  <td>PE5</td>
  <td>10.10.40.1</td>
  <td>49.0001.0000.0000.004e.00</td>
 </tr>
 <tr>
  <td>PE4</td>
  <td>10.10.50.1</td>
  <td>49.0001.0000.0000.005e.00</td>
 </tr>
 <tr>
  <td>PE6</td>
  <td>10.10.60.1</td>
  <td>49.0001.0000.0000.006e.00</td>
 </tr>
 <tr>
  <td>P1</td>
  <td>192.168.110.1</td>
  <td>49.0001.0000.0000.0010.00</td>
 </tr>
 <tr>
  <td>P2</td>
  <td>192.168.120.1</td>
  <td>49.0001.0000.0000.0020.00</td>
 </tr>
 <tr>
  <td>P3</td>
  <td>192.168.130.1</td>
  <td>49.0001.0000.0000.0030.00</td>
 </tr>
 <tr>
  <td>P4</td>
  <td>192.168.140.1</td>
  <td>49.0001.0000.0000.0040.00</td>
 </tr>
 <tr>
  <td>P5</td>
  <td>192.168.150.1</td>
  <td>49.0001.0000.0000.0050.00</td>
 </tr>
 <tr>
  <td>P6</td>
  <td>192.168.160.1</td>
  <td>49.0001.0000.0000.0060.00</td>
 </tr>
</table>

<!-- pagebreak -->

## Naming convention

The naming convention will decide which names will be used for each node in the Backbone.

As well as some of the protocols and other elements that needs a NC, to simplify

the general overview. 


In the MPLS backbone, AS 65000, 6 Provider Routers will be deploye, and 6 Provider Edge Routers. 
0
The naming matrix for each router is located at the buttom of this document.

All Routers reside in the same ISO, area ID 1. The system ID, will be 0000.0000.00**XX**, where the first **X** represents the Router number. 

The second **X** represents only Provider Edge routers.

**E.g. P1 will have a full ISO address of "49.0001.0000.0000.0010.00".**

**E.g. PE1 will have a full ISO address of "49.0001.0000.0000.001e.00".**

Each Router will have one or more AG (Aggragate Ethernet), peers. The ae**XX** will be set according to the peers. 

**E.g. P1 will have a "ae12" to the peer P2 router. Where "1" represents the P1 router and "2" represents the P2 router".**

**E.g. P3 will have a "ae35" to the peer P5 router. Where "3" represents the P3 router and "5" represents the P5 router".**

___

**Provider Routers:**

| Name | Equipment  | Explanation |
| ---- | ---------- | ----------- |
| P1   | SRX Router | P-Router    |
| P2   | SRX Router | P-Router    |
| P3   | SRX Router | P-Router    |
| P4   | SRX Router | P-Router    |
| P5   | SRX Router | P-Router    |
| P6   | SRX Router | P-Router    |

**Provider Edge Routers:**

| Name | Equipment  | Explanation                       |
| ---- | ---------- | --------------------------------- |
| PE1  | SRX Router | PE-Router, connectin to Europe    |
| PE2  | SRX Router | PE-Router, connectin to Brazil    |
| PE3  | SRX Router | PE-Router, connectin to Australia |
| PE4  | SRX Router | PE-Router, connectin to Asia      |
| PE5  | SRX Router | PE-Router, connectin to Africa    |
| PE6  | SRX Router | PE-Router, connectin to Russia    |