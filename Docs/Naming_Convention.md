The naming convention will decide which names will be used for each node in the Backbone.

As well as some of the protocols and other elements that needs a NC, to simplify

the general overview. 


In the MPLS backbone, AS 65000, 6 Provider Routers will be deploye, and 6 Provider Edge Routers. 

The naming matrix for each router is located at the buttom of this document.

All Routers reside in the same ISO, area ID 1. The system ID, will be 0000.0000.00**XX**, where the first **X** represents the Router number. 

The second **X** represents only Provider Edge routers.

**E.g. P1 will have a full ISO address of "49.0001.0000.0000.0010.00".**

**E.g. PE1 will have a full ISO address of "49.0001.0000.0000.001e.00".**

Each Router will have one or more AG (Aggragate Ethernet), peers. The ae**XX** will be set according to the peers. 

**E.g. P1 will have a "ae12" to the peer P2 router. Where "1" represents the P1 router and "2" represents the P2 router".**

**E.g. P3 will have a "ae35" to the peer P5 router. Where "3" represents the P3 router and "5" represents the P5 router".**

___

**Provider Routers:**

|Name|Equipment|Explanation|
|---|---|---|
|P1|SRX Router|P-Router|
|P2|SRX Router|P-Router|
|P3|SRX Router|P-Router|
|P4|SRX Router|P-Router|
|P5|SRX Router|P-Router|
|P6|SRX Router|P-Router|

**Provider Edge Routers:**

|Name|Equipment|Explanation|
|---|---|---|
|PE1|SRX Router|PE-Router, connectin to Europe|
|PE2|SRX Router|PE-Router, connectin to Brazil|
|PE3|SRX Router|PE-Router, connectin to Australia|
|PE4|SRX Router|PE-Router, connectin to Asia|
|PE5|SRX Router|PE-Router, connectin to Africa|
|PE6|SRX Router|PE-Router, connectin to Russia|


