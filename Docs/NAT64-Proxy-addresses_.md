# NAT64 Proxy addressing scheme.
  
<div class="pagebreak"> </div>
  
  
## Table of contents
  
- [NAT64 Proxy addressing scheme.](#nat64-proxy-addressing-scheme)
    - [Table of contents](#table-of-contents)
    - [Preface](#preface)
    - [IP scheme](#ip-scheme)
  
<div class="pagebreak"> </div>
  
  
## Preface
  
* This document will contain a table of the NDP and ARP proxy addresses used, and where they are used for static NAT.
  
* Table is found below.
  
<div class="pagebreak"> </div>
  
  
## IP scheme
  
| **Device** | **Proxy adress type** | **Proxy address** | **Inbound interface** | **Subnet** | **To server IP**      | **Outbound interface** |
| ---------- | --------------------- | ----------------- | ---------------- | ---------- | --------------------- | ----------------
| DNS_SRX    | NDP-PROXY (MPLS-DNS)             | 2001:0:2:20::3    | ge-0/0/0.0       | /64        | 10.69.69.2 (MPLS-DNS) | ge-0/0/1.0 
| DNS_SRX    | NDP-PROXY (WEB-DNS)             | 2001:0:2:20::4    | ge-0/0/0.0       | /64        | 10.69.68.2 (WEB-DNS | ge-0/0/2.0 
| DNS_SRX    | NDP-PROXY (WEBSERVER-Europe)             | 2001:0:2:20::10    | ge-0/0/0.0       | /64        | 10.69.67.10 ((WEBSERVER-Europe) | ge-0/0/3.0 
| DNS_SRX    | NDP-PROXY (WEBSERVER-Australia)             | 2001:0:2:20::30    | ge-0/0/0.0       | /64        | 10.69.67.3 (WEBSERVER-Australia) | ge-0/0/3.0 
| DNS_SRX    | NDP-PROXY (WEBSERVER-Asia)            | 2001:0:2:20::40   | ge-0/0/0.0       | /64        | 10.69.67.4 (WEBSERVER-Asia) | ge-0/0/3.0 
| DNS_SRX    | NDP-PROXY (WEBSERVER-Africa)            | 2001:0:2:20::50    | ge-0/0/0.0       | /64        | 10.69.67.5 (WEBSERVER-Africa) | ge-0/0/3.0 
| DNS_SRX    | NDP-PROXY (WEBSERVER-Russia)            | 2001:0:2:20::60    | ge-0/0/0.0       | /64        | 10.69.67.6 (WEBSERVER-Russia) | ge-0/0/3.0 
|   |           |    |      |       |  | 
| DNS_SRX    | ARP-PROXY (MPLS-DNS)            | 10.69.69.5        | ge-0/0/1.0       | /24        | 10.69.69.2 (MPLS-DNS) | ge-0/0/0.0
| DNS_SRX    | ARP-PROXY (WEB-DNS)            | 10.69.68.5        | ge-0/0/2.0       | /24        | 10.69.68.2 (WEB-DNS) | ge-0/0/0.0
| DNS_SRX    | ARP-PROXY (WEBSERVER-Europe)            | 10.69.67.100        | ge-0/0/3.0       | /24        | 10.69.67.10 (WEBSERVER-Europe) | ge-0/0/0.0
| DNS_SRX    | ARP-PROXY (WEBSERVER-Australia)            | 10.69.67.30        | ge-0/0/3.0       | /24        | 10.69.67.3 (WEBSERVER-Australia) | ge-0/0/0.0
| DNS_SRX    | ARP-PROXY (WEBSERVER-Asia)            | 10.69.67.40        | ge-0/0/3.0       | /24        | 10.69.67.4 (WEBSERVER-Asia) | ge-0/0/0.0
| DNS_SRX    | ARP-PROXY (WEBSERVER-Africa)            | 10.69.67.50        | ge-0/0/3.0       | /24        | 10.69.67.5 (WEBSERVER-Africa) | ge-0/0/0.0
| DNS_SRX    | ARP-PROXY (WEBSERVER-Russia)            | 10.69.67.60        | ge-0/0/3.0       | /24        | 10.69.67.6 (WEBSERVER-Russia) | ge-0/0/0.0
  