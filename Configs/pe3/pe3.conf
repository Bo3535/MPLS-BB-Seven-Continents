
## Last changed: 2018-11-26 08:11:49 UTC
version 12.1X46-D30.2;
system {
    host-name PE3;
    root-authentication {
        encrypted-password "$1$EWKwbAVR$Kvz7ctU8TN9cOA6UAAkXa1";
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMjBBOLaPrXer3CP2Ndu969vMGDCI8AdtAYK1r9YRvoOl/44QEEeEWCYUoe+nG+V22NacHunSnzGB3Za6zG606f6hjvL13IkhcvGUcfk2es6vD/lcReVgjyR7DbSLbAiCrK4/5uBCAe7viPlicxyDfAGx21x+4ZdDKrFcQgQvS/Z92DfhqKmUn1o52LW1OKcMAV9yt+n4SyXMIJS5VnXja+rgd15xBnfbB4vVlm4JfjO8j75KBFgAskQ3cCAQsctJScVbEmyvUnbDQRLk+MI6k5viJoSTREXUXOJlD+7bmB3PjdXuTgfvYQe4MPnOEbb5+dNsp3qCIeITW3z4nFMQf root@radius";
    }
    name-server {
        2001:0:2:20::3;
    }
    login {
        user benjamin {
            uid 2005;
            class super-user;
            authentication {
                encrypted-password "$1$ufsR6JjY$a7sjcZrmZ0GAKviPoVjzA.";
            }
        }
        user cecilie {
            uid 2006;
            class super-user;
            authentication {
                encrypted-password "$1$Wv5bhqeX$n5YpUJDGVCTB.rwioDfQq0";
            }
        }
        user lasse {
            uid 2002;
            class super-user;
            authentication {
                encrypted-password "$1$BuyBNG3c$RoJA5KllmFsW7QufRFuDO0";
            }
        }
        user marian {
            uid 2007;
            class super-user;
            authentication {
                encrypted-password "$1$HWDV2BJQ$jUF0WWSdzSoxrYnbINJP./";
            }
        }
    }
    services {
        ssh;
        netconf {
            ssh;
        }
    }
    max-configurations-on-flash 5;
    max-configuration-rollbacks 5;
    license {
        autoupdate {
            url https://ae1.juniper.net/junos/key_retrieval;
        }
    }
}
chassis {
    aggregated-devices {
        ethernet {
            device-count 128;
        }
    }
}
interfaces {
    ge-0/0/0 {
        unit 0 {
            family inet {
                address 10.210.14.177/24;
            }
        }
    }
    ge-0/0/1 {
        gigether-options {
            802.3ad ae33;
        }
    }
    ge-0/0/2 {
        gigether-options {
            802.3ad ae33;
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 192.168.100.5/30;
            }
            family iso;
            family inet6;
            family mpls;
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet6 {
                address 2001:0:3:8::/127;
            }
            family mpls;
        }
    }
    ae33 {
        aggregated-ether-options {
            minimum-links 1;
            link-speed 1g;
            lacp {
                active;
                periodic fast;
            }
        }
        unit 0 {
            family inet {
                address 192.168.75.5/30;
            }
            family iso;
            family inet6;
            family mpls;
        }
    }
    lo0 {
        unit 0 {
            family inet {
                address 10.10.30.1/32;
            }
            family iso {
                address 49.0001.0000.0000.003e.00;
            }
            family mpls;
        }
    }
}
routing-options {
    router-id 10.10.30.1;
    autonomous-system 65000;
}
protocols {
    rsvp {
        interface ae33.0;
        interface ge-0/0/3.0;
    }
    mpls {
        explicit-null;
        ipv6-tunneling;
        label-switched-path PE3-to-PE1 {
            from 10.10.30.1;
            to 10.10.10.1;
            bandwidth 50m;
        }
        label-switched-path PE3-to-PE2 {
            from 10.10.30.1;
            to 10.10.20.1;
            bandwidth 50m;
        }
        label-switched-path PE3-to-PE4 {
            from 10.10.30.1;
            to 10.10.40.1;
            bandwidth 50m;
        }
        label-switched-path PE3-to-PE5 {
            from 10.10.30.1;
            to 10.10.50.1;
            bandwidth 50m;
        }
        label-switched-path PE3-to-PE6 {
            from 10.10.30.1;
            to 10.10.60.1;
            bandwidth 50m;
        }
        interface lo0.0;
        interface ae33.0;
        interface ge-0/0/3.0;
        interface ge-0/0/4.0;
    }
    bgp {
        group external-peers {
            type external;
            family inet6 {
                unicast;
            }
            peer-as 65003;
            neighbor 2001:0:3:8::1;
        }
        group internal-peers {
            type internal;
            local-address 10.10.30.1;
            family inet6 {
                labeled-unicast {
                    explicit-null;
                }
            }
            export [ next-hop-self send-v6 ];
            local-as 65000;
            neighbor 10.10.10.1;
            neighbor 10.10.20.1;
            neighbor 10.10.40.1;
            neighbor 10.10.50.1;
            neighbor 10.10.60.1;
            neighbor 192.168.110.1;
            neighbor 192.168.120.1;
            neighbor 192.168.140.1;
            neighbor 192.168.150.1;
            neighbor 192.168.160.1;
        }
    }
    isis {
        interface ge-0/0/3.0 {
            level 1 disable;
        }
        interface ae33.0 {
            level 1 disable;
        }
        interface lo0.0 {
            level 1 disable;
        }
    }
}
policy-options {
    policy-statement next-hop-self {
        then {
            next-hop self;
        }
    }
    policy-statement send-bgp6 {
        from {
            family inet6;
            protocol bgp;
        }
        then accept;
    }
    policy-statement send-isis {
        from {
            family inet;
            protocol isis;
        }
        then accept;
    }
    policy-statement send-v6 {
        from protocol direct;
        then accept;
    }
}
security {
    forwarding-options {
        family {
            inet6 {
                mode packet-based;
            }
            mpls {
                mode packet-based;
            }
            iso {
                mode packet-based;
            }
        }
    }
}
